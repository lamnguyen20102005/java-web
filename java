sudo vi /etc/profile

#Add the following lines at the end of the file:
export JAVA_HOME=/usr/lib/jvm/java-1.8.0-openjdk
export PATH=$PATH:$JAVA_HOME/bin

#Load the updated /etc/profile file:
source /etc/profile

#Verify the JAVA_HOME environment variable:
echo $JAVA_HOME
