import java.io.*;
import java.net.InetSocketAddress;
import java.nio.charset.StandardCharsets;
import java.net.URLDecoder;
import com.sun.net.httpserver.*;

public class WebServer {
    public static void main(String[] args) throws IOException {
        HttpServer server = HttpServer.create(new InetSocketAddress(80), 0);
        server.createContext("/", new LoginHandler());
        server.setExecutor(null);
        server.start();
        System.out.println("Server is running on port 80");
    }

    static class LoginHandler implements HttpHandler {
        @Override
        public void handle(HttpExchange exchange) throws IOException {
            if (exchange.getRequestMethod().equalsIgnoreCase("POST")) {
                String formData = readRequestBody(exchange);
                String username = getFormValue(formData, "username");
                String password = getFormValue(formData, "password");

                if (isValidCredentials(username, password)) {
                    sendTicTacToePage(exchange);
                } else {
                    sendLoginForm(exchange, "Invalid username or password");
                }
            } else {
                sendLoginForm(exchange, null);
            }
        }

        private String readRequestBody(HttpExchange exchange) throws IOException {
            InputStreamReader isr = new InputStreamReader(exchange.getRequestBody(), StandardCharsets.UTF_8);
            BufferedReader br = new BufferedReader(isr);
            return br.readLine();
        }

        private String getFormValue(String formData, String fieldName) throws UnsupportedEncodingException {
            String[] params = formData.split("&");
            for (String param : params) {
                String[] keyValue = param.split("=");
                if (keyValue.length == 2) {
                    String key = URLDecoder.decode(keyValue[0], "UTF-8");
                    String value = URLDecoder.decode(keyValue[1], "UTF-8");
                    if (key.equals(fieldName)) {
                        return value;
                    }
                }
            }
            return null;
        }

        private boolean isValidCredentials(String username, String password) {
            return username != null && password != null && username.equals("Lam") && password.equals("123");
        }

        private void sendLoginForm(HttpExchange exchange, String errorMessage) throws IOException {
            StringBuilder htmlBuilder = new StringBuilder();
            htmlBuilder.append("<html><body style='display: flex; justify-content: center; align-items: center; height: 100vh;'>")
                    .append("<div style='border: 1px solid black; padding: 20px; text-align: center;'>")
                    .append("<h1>Login</h1>");

            if (errorMessage != null) {
                htmlBuilder.append("<p style='color: red;'>").append(errorMessage).append("</p>");
            }

            htmlBuilder.append("<form method='post' action='/'>")
                    .append("<div style='display: flex; justify-content: center; align-items: center; flex-direction: column;'>")
                    .append("<div style='margin-bottom: 10px;'><label for='username'>Username:</label> <input type='text' name='username' id='username' style='width: 200px;'></div>")
                    .append("<div><label for='password'>Password:</label> <input type='password' name='password' id='password' style='width: 200px;'></div>")
                    .append("<input type='submit' value='Login'>")
                    .append("</div></form>")
                    .append("</div></body></html>");

            String htmlResponse = htmlBuilder.toString();
            byte[] responseBytes = htmlResponse.getBytes(StandardCharsets.UTF_8);
            exchange.sendResponseHeaders(200, responseBytes.length);
            OutputStream outputStream = exchange.getResponseBody();
            outputStream.write(responseBytes);
            outputStream.close();
        }
        
        private void sendTicTacToePage(HttpExchange exchange) throws IOException {
    String htmlResponse = "<html><head><style>"
            + "table { border-collapse: collapse; }"
            + "td { width: 100px; height: 100px; text-align: center; font-size: 24px; cursor: pointer; border: 1px solid black; }"
            + "</style></head><body><h1>Tic Tac Toe</h1>"
            + "<table id='tic-tac-toe'>"
            + "<tr><td onclick='makeMove(0, 0)'></td><td onclick='makeMove(0, 1)'></td><td onclick='makeMove(0, 2)'></td></tr>"
            + "<tr><td onclick='makeMove(1, 0)'></td><td onclick='makeMove(1, 1)'></td><td onclick='makeMove(1, 2)'></td></tr>"
            + "<tr><td onclick='makeMove(2, 0)'></td><td onclick='makeMove(2, 1)'></td><td onclick='makeMove(2, 2)'></td></tr>"
            + "</table>"
            + "<script>"
            + "var currentPlayer = 'X';"
            + "var cells = document.getElementsByTagName('td');"
            + "var board = [['', '', ''], ['', '', ''], ['', '', '']];"
            + "function makeMove(row, col) {"
            + "  if (board[row][col] === '') {"
            + "    board[row][col] = currentPlayer;"
            + "    cells[row * 3 + col].innerText = currentPlayer;"
            + "    currentPlayer = currentPlayer === 'X' ? 'O' : 'X';"
            + "    checkWin();"
            + "  }"
            + "}"
            + "function checkWin() {"
            + "  for (var i = 0; i < 3; i++) {"
            + "    if (board[i][0] !== '' && board[i][0] === board[i][1] && board[i][1] === board[i][2]) {"
            + "      redirectToYouTube();"
            + "      return;"
            + "    }"
            + "    if (board[0][i] !== '' && board[0][i] === board[1][i] && board[1][i] === board[2][i]) {"
            + "      redirectToYouTube();"
            + "      return;"
            + "    }"
            + "  }"
            + "  if (board[0][0] !== '' && board[0][0] === board[1][1] && board[1][1] === board[2][2]) {"
            + "    redirectToYouTube();"
            + "    return;"
            + "  }"
            + "  if (board[0][2] !== '' && board[0][2] === board[1][1] && board[1][1] === board[2][0]) {"
            + "    redirectToYouTube();"
            + "  }"
            + "}"
            + "function redirectToYouTube() {"
            + "  window.location.replace('https://www.youtube.com/');"
            + "}"
            + "</script>"
            + "</body></html>";

    byte[] responseBytes = htmlResponse.getBytes(StandardCharsets.UTF_8);
    exchange.sendResponseHeaders(200, responseBytes.length);
    OutputStream outputStream = exchange.getResponseBody();
    outputStream.write(responseBytes);
    outputStream.close();
}
    }
}
