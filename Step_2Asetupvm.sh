#Update the system:
sudo yum update

#Install Java Development Kit (JDK) using yum:
sudo yum install java-1.8.0-openjdk-devel

#Verify the Java installation:
java -version

# For Node.js 18.x (LTS)
curl -sL https://rpm.nodesource.com/setup_18.x | sudo bash -
sudo yum install -y nodejs

#Verify the installation:
node -v
npm -v

#Install the C++ compiler (GCC): 
sudo yum install gcc

#nstall the C++ standard library: 
sudo yum install gcc-c++

#Set up a build system (optional):
sudo yum install make

git config --global user.email "lamnguyen20102005@gmail.com"
git config --global user.name "lamnguyen20102005"

#Install the PostgreSQL client package:
sudo yum install postgresql

#Optionally, you can also install the additional "contrib" package, which includes extra utilities and extensions:
sudo yum install postgresql-contrib


